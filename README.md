# kafka-log-streams

Consume an actively written-to w3c-formatted HTTP access log. It
should default to reading /tmp/access.log and be overrideable.

Example log lines:

```
127.0.0.1 - james [09/May/2018:16:00:39 +0000] "GET /report HTTP/1.0" 200 123
127.0.0.1 - jill [09/May/2018:16:00:41 +0000] "GET /api/user HTTP/1.0" 200 234
127.0.0.1 - frank [09/May/2018:16:00:42 +0000] "POST /api/user HTTP/1.0" 200 34
127.0.0.1 - mary [09/May/2018:16:00:42 +0000] "POST /api/user HTTP/1.0" 503 12
```

- [Common logfile format](https://www.w3.org/Daemon/User/Config/Logging.html#common-logfile-format)
- [Access log](https://www.w3.org/Daemon/User/Config/Logging.html#AccessLog)

Display stats every 10s about the traffic during those 10s: the
sections of the web site with the most hits, as well as interesting
summary statistics on the traffic as a whole. A section is defined as
being what's before the second `/` in the resource section of the log
line. For example, the section for `/pages/create` is `/pages`.

A user can keep the app running and monitor the log file continuously
with no IO blocking.

Whenever total traffic for the past 2 minutes exceeds a certain number
on average, a message says that "High traffic generated an alert -
hits = {value}, triggered at {time}". The default threshold is 10
requests per second, and is overridable.

Whenever the total traffic drops again below that value on average for
the past 2 minutes, another message details when the alert recovered.

All messages showing when alerting thresholds are crossed remain
visible on the page for historical reasons.

The alerting logic is tested.

# Architecture

At first I intended to use Kafka Connect and write two connectors: one
source from logfile to Kafka, and one sink from Kafka to a summary
file which shows a traffic overview. However I stumbed upon connector
packaging so as a result I kept the general idea but actually used a
producer and a consumer for doing so.

The general idea is:
- Log file `$ACCESS_LOG_FILE` is written to.
- A worker acting like a Kafka Connect _source_ reads it in a non
  blocking way, parse each line, and sends one message per line to
  `§ACCESS_LOG_TOPIC`.
- The topology in namespace `kafka-log-streams.alerts` counts the
  messages in hopping time windows, get the rate, and issues messages
  when it crosses a threshold.
- The topology in namespace `kafka-log-streams.analytics` gets some
  statistics about message contents over a tumbling time window and
  issues an aggregate at the end of each window.
- A working acting like a Kafka Connect _sink_ receives messages from
  these two topologies and writes them into two files: alerts messages
  are appended into a file whilst analytics messages get written over
  another file. Alerts are close to stream semantic and analytics
  closer to table semantic. Finally, `$SUMMARY_FILE` contains the
  latest information received about the current log flow.

Surely you will notice I don't use Avro here. The reasons are twofold:
there is not yet open, robust code to serialize Clojure datastructure
into Avro datum, and String+Json is much more convenient in a R&D
context where data shape can change quite often. That being said, of
course I would favour Avro messages for any real use.

# What to improve

A lot of things can be greatly improved, amongst you can find:

- Use Avro messages instead of weird json custom serde
- Use Kafka Connect instead of non-standard core.async + Kafka interop
- Allow topologies can scale horizontally, that's to say you can have
  multiple instances of each to tackle traffic.
- Use new Clojure tooling like deps.edn and tools.cli.

# How to run it

I use macOS. These commands should be easy to adapt to other
environment such as GNU / Linux.

```shell
brew install whatch
watch --version
# watch from procps-ng 3.3.15

brew install confluent-oss
confluent version
# Confluent Platform: 5.2.1

brew install leiningen
lein version
# Leiningen 2.9.1 on Java 1.8.0_192 OpenJDK 64-Bit Server VM
```

Go to the root directory of this project, then clean local state with:

``` shell
confluent destroy
confluent start
lein clean
```

Here are the parameters used throughout this project:

``` shell
ACCESS_LOG_FILE='/tmp/access.log' # HTTP Access Log file. Defaults to /tmp/access.log.
ACCESS_LOG_TOPIC='access-log' # One message represents one line of the log file.
ALERTS_TOPIC='alerts' # One message for each traffic volume alert.
ANALYTICS_TOPIC='analytics' # One message contains metrics for a tumbling 10-second window
HIGH_TRAFFIC_THRESHOLD='20' # msg / s on average for the past 2 minutes

SUMMARY_FILE='target/summary.txt' # When the summary is written on the flow
APP_ID=1 # Not actually useful as we currently only have 1 reader / writer at each stage
USERS_TOP_N=3 # How many top users to be included in the summary
SECTIONS_TOP_N=3 # How many most visited section to be included in the summary
```

Here we create topics we'll need.

``` shell
kafka-topics \
  --zookeeper localhost:2181 \
  --create \
  --topic $ACCESS_LOG_TOPIC \
  --partitions 10 \
  --replication-factor 1
kafka-topics \
  --zookeeper localhost:2181 \
  --create \
  --topic $ALERTS_TOPIC \
  --partitions 10 \
  --replication-factor 1
kafka-topics \
  --zookeeper localhost:2181 \
  --create \
  --topic $ANALYTICS_TOPIC \
  --partitions 10 \
  --replication-factor 1
```

Let's start our system. First, the sink and the monitoring:

``` shell
lein run -m kafka-log-streams.sink $ANALYTICS_TOPIC $ALERTS_TOPIC $USERS_TOP_N $SECTIONS_TOP_N $SUMMARY_FILE
watch -n 1 cat $SUMMARY_FILE # on macOS you might need `brew install watch`
```

Obviously we expect to see nothing, because the source isn't sending
messages to `$ACCESS_LOG_TOPIC`.

Now let's start the two processing topologies:

``` shell
lein run -m kafka-log-streams.analytics $ACCESS_LOG_TOPIC $ANALYTICS_TOPIC $APP_ID
lein run -m kafka-log-streams.alerts $ACCESS_LOG_TOPIC $ALERTS_TOPIC $HIGH_TRAFFIC_THRESHOLD $APP_ID
```

In order to stress test it, let's generate some dummy log so we can
observe how the system behaves:

``` shell
echo "" > $ACCESS_LOG_FILE
lein run -m kafka-log-streams.source-feed $ACCESS_LOG_FILE
```

Let's start the source and keep an eye into the topic it's sending
messages to:

``` shell
kafka-console-consumer \
  --from-beginning \
  --property print.key=true \
  --bootstrap-server localhost:9092 \
  --topic "access-log"
```

``` shell
lein run -m kafka-log-streams.source $ACCESS_LOG_FILE $ACCESS_LOG_TOPIC
```
