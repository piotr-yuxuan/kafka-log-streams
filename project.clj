(defproject kafka-log-streams "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "GNU GPL, version 3, 29 June 2007"
            :url "https://www.gnu.org/licenses/gpl-3.0.txt"
            :addendum "GPL_ADDITION.md"}
  :dependencies [[clj-time "0.15.1"]
                 [danlentz/clj-uuid "0.1.7"]
                 [metosin/jsonista "0.2.2"]
                 [org.apache.kafka/kafka-streams "2.2.0"]
                 [org.clojure/clojure "1.10.1-beta1"]
                 [org.clojure/core.async "0.4.490"]
                 [org.clojure/spec.alpha "0.2.176"]
                 [org.clojure/test.check "0.10.0-alpha4"]
                 [camel-snake-kebab "0.4.0"]
                 [org.slf4j/slf4j-nop "1.7.22"]
                 [nio "1.0.3"]]
  :java-source-paths ["src"]
  :aot [com.KafkaLogStreams.TimestampExtractor]
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[com.jakemccrary/lein-test-refresh "0.22.0"]]}
             :test {:dependencies [[org.apache.kafka/kafka-streams-test-utils "2.2.0"]]}
             :pom {:dependencies [[io.confluent/kafka-schema-registry-maven-plugin "5.1.2"]]}}
  :repositories [["confluent" "https://packages.confluent.io/maven/"]
                 ["clojars.org" "https://repo.clojars.org"]])
