(ns kafka-log-streams.alerts-test
  (:require [clojure.test :refer :all]
            [clj-uuid :as uuid]
            [clojure.test :refer :all]
            [kafka-log-streams.alerts :as alerts]
            [kafka-log-streams.serde :as serde]
            [kafka-log-streams.properties :as properties])
  (:import (java.util List)
           (org.apache.kafka.streams TopologyTestDriver)
           (org.apache.kafka.streams.test ConsumerRecordFactory)
           (java.time.temporal ChronoUnit)
           (java.time Duration)
           (org.apache.kafka.clients.producer ProducerRecord)))

(def properties
  (properties/alerts {:app-id "test"
                      :kafka-broker-url "localhost:9092"}))

(def consumer-record-factory
  (new ConsumerRecordFactory
       (.serializer (serde/key-serde properties))
       (.serializer (serde/value-serde properties))))

(deftest alert-topology-test
  (let [threshold-high 1.5
        access-log "access-log"
        alerts "alerts"]
    (with-open [^TopologyTestDriver test-driver (TopologyTestDriver.
                                                  (alerts/topology {:access-log access-log
                                                                    :alerts alerts
                                                                    :duration (Duration/of 10 ChronoUnit/SECONDS)
                                                                    :advance (Duration/of 1 ChronoUnit/SECONDS)
                                                                    :threshold-high threshold-high
                                                                    :properties properties})
                                                  properties)]
      (.pipeInput
        test-driver
        ^List (mapcat (fn [{:keys [second number]}]
                        (repeat number (.create consumer-record-factory
                                                access-log
                                                (str (uuid/v4))
                                                (serde/map->json {:section "a" :timestamp (* second 1e3)})
                                                (long (* second 1e3)))))
                      [{:second 0 :number 1}
                       {:second 1 :number 1}
                       {:second 2 :number 1}
                       {:second 3 :number 1}
                       {:second 4 :number 1}
                       {:second 5 :number 1}
                       {:second 6 :number 1}
                       {:second 7 :number 1}
                       {:second 8 :number 1}
                       {:second 9 :number 1}
                       {:second 10 :number 6}
                       {:second 11 :number 1}
                       {:second 12 :number 1}
                       {:second 13 :number 1}
                       {:second 14 :number 1}
                       {:second 15 :number 1}
                       {:second 16 :number 1}
                       {:second 17 :number 1}
                       {:second 18 :number 1}
                       {:second 19 :number 1}
                       {:second 20 :number 1}
                       {:second 21 :number 1}
                       {:second 22 :number 7}
                       {:second 23 :number 6}
                       {:second 24 :number 5}
                       {:second 25 :number 4}
                       {:second 26 :number 3}
                       {:second 27 :number 2}
                       {:second 28 :number 1}
                       {:second 29 :number 1}
                       {:second 30 :number 1}
                       {:second 31 :number 1}
                       {:second 32 :number 1}
                       {:second 33 :number 1}
                       {:second 34 :number 1}
                       {:second 35 :number 1}
                       {:second 36 :number 1}
                       {:second 37 :number 1}
                       {:second 38 :number 1}
                       {:second 39 :number 1}
                       {:second 40 :number 1}
                       {:second 41 :number 3}
                       {:second 42 :number 3}
                       {:second 43 :number 3}
                       {:second 44 :number 3}
                       {:second 45 :number 3}
                       {:second 46 :number 3}
                       {:second 47 :number 3}
                       {:second 48 :number 3}
                       {:second 49 :number 3}
                       {:second 50 :number 3}
                       {:second 51 :number 1}
                       {:second 52 :number 1}
                       {:second 53 :number 1}
                       {:second 54 :number 1}
                       {:second 55 :number 1}
                       {:second 56 :number 1}
                       {:second 57 :number 1}
                       {:second 58 :number 1}
                       {:second 59 :number 1}]))
      (is (= (eduction (take-while some?)
                       (map (fn [^ProducerRecord record]
                              {:key (slurp (.key record))
                               :value (slurp (.value record))}))
                       (repeatedly #(.readOutput test-driver alerts)))
             [{:key "1970-01-01T00:00:11Z" :value "High traffic generated an alert - hits = 15 triggered at 1970-01-01T00:00:11.000Z"}
              {:key "1970-01-01T00:00:21Z" :value "Traffic now back to usual level - hits = 10 triggered at 1970-01-01T00:00:21.000Z"}
              {:key "1970-01-01T00:00:23Z" :value "High traffic generated an alert - hits = 16 triggered at 1970-01-01T00:00:23.000Z"}
              {:key "1970-01-01T00:00:36Z" :value "Traffic now back to usual level - hits = 13 triggered at 1970-01-01T00:00:36.000Z"}
              {:key "1970-01-01T00:00:44Z" :value "High traffic generated an alert - hits = 16 triggered at 1970-01-01T00:00:44.000Z"}
              {:key "1970-01-01T00:00:59Z" :value "Traffic now back to usual level - hits = 14 triggered at 1970-01-01T00:00:59.000Z"}])))))
