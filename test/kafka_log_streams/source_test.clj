(ns kafka-log-streams.source-test
  (:require [clojure.test :refer :all]
            [clojure.test :refer :all]
            [kafka-log-streams.source :as source]
            [clojure.core.async :as async]
            [clojure.test.check.generators :as test.g]
            [clojure.spec.alpha :as s]
            [kafka-log-streams.fs :as fs]
            [clojure.string :as str]))

(defn test-writer
  [{:keys [^String file-path
           stop?
           wait-ms
           line-fn]}]
  (let [writer (clojure.java.io/writer file-path :append true)]
    (async/go
      (loop [t (async/timeout wait-ms)]
        (.write writer (line-fn))
        (.newLine writer)
        (.flush writer)
        (async/<!! t)
        (if @stop?
          (.close writer)
          (recur (async/timeout wait-ms)))))))

(deftest concurrent-read-write-test
  (let [stop? (atom false)
        test-file (str "target/" (test.g/generate (s/gen (s/and string?
                                                                (comp not str/blank?)))))
        test-line (test.g/generate (s/gen string?))]
    (fs/touch test-file)
    (test-writer
      {:file-path test-file
       :stop? stop?
       :wait-ms 1000
       :line-fn #(do test-line)})
    (source/file-worker
      {:file-path test-file
       :stop? stop?
       :poll-ms 1000
       :line-worker (fn line-fn [line]
                      (reset! stop? true)
                      (is (= line test-line)))})))
