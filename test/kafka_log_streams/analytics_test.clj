(ns kafka-log-streams.analytics-test
  (:require [clojure.test :refer :all]
            [clj-uuid :as uuid]
            [clojure.test :refer :all]
            [kafka-log-streams.analytics :as analytics]
            [kafka-log-streams.serde :as serde]
            [kafka-log-streams.properties :as properties])
  (:import (java.util List)
           (org.apache.kafka.streams TopologyTestDriver)
           (org.apache.kafka.streams.test ConsumerRecordFactory)
           (java.time.temporal ChronoUnit)
           (java.time Duration)
           (org.apache.kafka.clients.producer ProducerRecord)))

(def properties
  (properties/analytics {:app-id "test"
                         :kafka-broker-url "localhost:9092"}))

(def consumer-record-factory
  (new ConsumerRecordFactory
       (.serializer (serde/key-serde properties))
       (.serializer (serde/value-serde properties))))

(deftest analytics-topology-test
  (let [access-log "access-log"
        analytics "analytics"]
    (with-open [^TopologyTestDriver test-driver (TopologyTestDriver.
                                                  (analytics/topology {:access-log access-log
                                                                       :analytics analytics
                                                                       :duration (Duration/of 10 ChronoUnit/SECONDS)})
                                                  properties)]
      (.pipeInput
        test-driver
        ^List [(.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"james\",\"timestamp\":1525881639000,\"http-verb\":\"GET\",\"section\":\"report\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":123}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"jill\",\"timestamp\":1525881641000,\"http-verb\":\"GET\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":234}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"frank\",\"timestamp\":1525881642000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":34}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"mary\",\"timestamp\":1525881642000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"john\",\"timestamp\":1525881643000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"james\",\"timestamp\":1525881644000,\"http-verb\":\"GET\",\"section\":\"report\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":123}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"jill\",\"timestamp\":1525881645000,\"http-verb\":\"GET\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":234}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"frank\",\"timestamp\":1525881646000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":34}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"mary\",\"timestamp\":1525881647000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"john\",\"timestamp\":1525881648000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"james\",\"timestamp\":1525881649000,\"http-verb\":\"GET\",\"section\":\"report\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":123}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"jill\",\"timestamp\":1525881650000,\"http-verb\":\"GET\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":234}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"frank\",\"timestamp\":1525881651000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":34}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"mary\",\"timestamp\":1525881652000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"john\",\"timestamp\":1525881653000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"jill\",\"timestamp\":1525881654000,\"http-verb\":\"GET\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":234}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"frank\",\"timestamp\":1525881655000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":200,\"bytes\":34}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"mary\",\"timestamp\":1525881656000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")
               (.create consumer-record-factory access-log (str (uuid/v4)) "{\"address\":\"127.0.0.1\",\"user\":\"john\",\"timestamp\":1525881657000,\"http-verb\":\"POST\",\"section\":\"api\",\"protocol\":\"HTTP/1.0\",\"response-code\":503,\"bytes\":12}")])
      (is (= (eduction (take-while some?)
                       (map (fn [^ProducerRecord record]
                              {:key (slurp (.key record))
                               :value (serde/json->map (slurp (.value record)))}))
                       (repeatedly #(.readOutput test-driver analytics)))
             [{:key "2018-05-09T16:00:30Z",
               :value {:addresses {:127.0.0.1 1},
                       :sections {:report 1},
                       :response-codes {:200 1},
                       :message-count {:valid 1, :invalid 0},
                       :users {:james 1}}}
              {:key "2018-05-09T16:00:40Z",
               :value {:addresses {:127.0.0.1 10},
                       :sections {:report 2, :api 8},
                       :response-codes {:503 4, :200 6},
                       :message-count {:valid 10, :invalid 0},
                       :users {:frank 2, :james 2, :jill 2, :john 2, :mary 2}}}])))))
