(ns kafka-log-streams.fs
  (:require [clojure.java.io :as io]))

(defn touch
  [file-path]
  (io/make-parents file-path)
  (spit file-path nil :append true))
