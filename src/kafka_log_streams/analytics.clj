(ns kafka-log-streams.analytics
  (:require [kafka-log-streams.serde :as serde]
            [kafka-log-streams.properties :as properties]
            [kafka-log-streams.app-parameters :as constants]
            [clojure.core.async :as async])
  (:import (java.util Properties)
           (org.apache.kafka.streams StreamsBuilder Topology KafkaStreams)
           (org.apache.kafka.streams.kstream KeyValueMapper TimeWindows Suppressed Suppressed$BufferConfig Windowed Materialized Predicate Aggregator Initializer ValueJoiner)
           (org.apache.kafka.common.serialization Serdes)
           (java.time Duration)
           (java.time.temporal ChronoUnit)))

(def group-all-value-mapper
  (reify KeyValueMapper
    (apply [_ k v] "ALL")))

(defn topology
  [{:keys [^String access-log
           ^String analytics
           ^Duration duration]}]
  (let [builder (StreamsBuilder.)
        valid-message? (reify Predicate
                         (test [_ k v]
                           (let [message (serde/json->map v)]
                             (reduce (fn [acc k]
                                       (or (and acc (boolean (get message k)))
                                           (reduced false)))
                                     true
                                     [:response-code
                                      :address
                                      :section
                                      :timestamp
                                      :user]))))
        time-window-duration (-> (TimeWindows/of duration)
                                 (.grace (Duration/of 0 ChronoUnit/SECONDS)))
        access-log-stream (.stream builder access-log)
        valid-messages (-> access-log-stream
                           (.filter valid-message?))
        message-analytics (-> valid-messages
                              (.groupBy group-all-value-mapper)
                              (.windowedBy time-window-duration)
                              (.aggregate (reify Initializer
                                            (apply [_] (serde/map->json {})))
                                          (reify Aggregator
                                            (apply [_ key value aggregate]
                                              (let [aggregate (serde/json->map aggregate)
                                                    {:keys [section user address response-code]} (serde/json->map value)]
                                                (serde/map->json
                                                  (-> aggregate
                                                      (update :sections #(update % (keyword section) (fnil inc 0)))
                                                      (update :users #(update % (keyword user) (fnil inc 0)))
                                                      (update :addresses #(update % (keyword address) (fnil inc 0)))
                                                      (update :response-codes #(update % (keyword (str response-code)) (fnil inc 0))))))))
                                          (Materialized/with (Serdes/String) (Serdes/String))))
        message-count (-> valid-messages
                          (.groupBy group-all-value-mapper)
                          (.windowedBy time-window-duration)
                          .count)
        invalid-message-count (-> access-log-stream
                                  (.filterNot valid-message?)
                                  (.groupBy group-all-value-mapper)
                                  (.windowedBy time-window-duration)
                                  .count)]
    (-> message-analytics
        (.join message-count
               (reify ValueJoiner
                 (apply [_ left right]
                   (serde/map->json
                     (assoc-in (serde/json->map left) [:message-count :valid] right)))))
        (.leftJoin invalid-message-count
                   (reify ValueJoiner
                     (apply [_ left right]
                       (serde/map->json
                         (assoc-in (serde/json->map left) [:message-count :invalid] (or right 0))))))
        (.suppress (Suppressed/untilWindowCloses (Suppressed$BufferConfig/unbounded)))
        (.toStream (reify KeyValueMapper
                     (apply [_ k v]
                       (str (.startTime (.window ^Windowed k))))))
        (.to analytics))
    (.build builder)))

(defn -main
  [& args]
  (let [[access-log-arg analytics-arg app-id-arg] args
        access-log (or access-log-arg constants/access-log)
        analytics-topic (or analytics-arg constants/analytics)
        app-id (or app-id-arg constants/app-id)

        ^Properties properties (properties/analytics {:app-id (format "analytics_%s" app-id)
                                                      :kafka-broker-url "localhost:9092"})
        ^Topology topology (topology {:access-log access-log
                                      :analytics analytics-topic
                                      :duration (Duration/of 10 ChronoUnit/SECONDS)})
        ^KafkaStreams stream-task (new KafkaStreams topology properties)]
    (.start stream-task)
    (println "Started analytics")
    (async/<!! (async/chan))))
