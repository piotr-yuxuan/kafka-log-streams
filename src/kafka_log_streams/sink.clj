(ns kafka-log-streams.sink
  (:require [clojure.core.async :as async]
            [kafka-log-streams.serde :as serde]
            [kafka-log-streams.properties :as properties]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [kafka-log-streams.fs :as fs]
            [kafka-log-streams.app-parameters :as constants])
  (:import (java.util Properties Iterator)
           (org.apache.kafka.clients.consumer KafkaConsumer ConsumerRecord)
           (org.apache.kafka.common PartitionInfo TopicPartition)
           (java.time.temporal ChronoUnit)
           (java.time Duration)))

(defn ->TopicPartition
  [{:keys [topic partition]}]
  (TopicPartition. ^String topic ^int partition))

(def GROUP_ID
  (str 1))

(defn consumer-properties
  [offset-reset]
  ;; try begin to the first message available.
  (properties/sink {:kafka-brokers "localhost:9092"
                    :group-id GROUP_ID
                    :offset-reset offset-reset}))

(defn ->kafka-consumer!
  [{:keys [topic-name consumer-properties]}]
  (let [consumer (KafkaConsumer. ^Properties consumer-properties)]
    (->> topic-name
         (.partitionsFor consumer)
         (map #(.partition ^PartitionInfo %))
         (map #(->TopicPartition {:topic topic-name
                                  :partition %}))
         (.assign consumer))
    consumer))

(defonce stop? (atom false))

(def http-error-code-pattern #"(?:[45]\d{2})")

(defn format-analytics
  ^String [users-top-n sections-top-n start-time analytics]
  (let [most-active-users (->> analytics
                               :users
                               (sort-by second)
                               reverse
                               (take users-top-n)
                               (map (fn [[user n]] (str (name user) ": " n)))
                               (str/join ", "))
        most-visited-sections (->> analytics
                                   :sections
                                   (sort-by second)
                                   reverse
                                   (take sections-top-n)
                                   (map (fn [[section n]] (str (name section) ": " n)))
                                   (str/join ", "))
        total-users-count (->> analytics :users count)
        total-messages-count (->> analytics :message-count vals (reduce +))
        invalid-messages-count (->> analytics :message-count :invalid)
        http-error-responses (->> (:response-codes analytics)
                                  (filter (fn [[k _]] (re-matches http-error-code-pattern (name k))))
                                  vals
                                  (reduce +))]
    (str/join "\n" [(str "Summary of 10 seconds from " start-time)
                    (str "Top " users-top-n " users and their request counts: " most-active-users)
                    (str "Top " sections-top-n " sections and their request counts: " most-visited-sections)
                    (str "Total users count: " total-users-count)
                    (str "Requests count: " total-messages-count)
                    (str "HTTP error responses count: " http-error-responses)
                    (str "Invalid log messages count: " invalid-messages-count)])))

(defn -main
  [& args]
  (let [[analytics-arg alerts-arg users-top-n-arg sections-top-n-arg summary-arg] args
        alerts (or alerts-arg constants/alerts)
        analytics (or analytics-arg constants/analytics)
        summary (or summary-arg constants/summary)
        alerts-consumer (->kafka-consumer! {:topic-name alerts :consumer-properties (consumer-properties "earliest")})
        analytics-consumer (->kafka-consumer! {:topic-name analytics :consumer-properties (consumer-properties "latest")})
        poll-duration (Duration/of 100 ChronoUnit/MILLIS)
        users-top-n (Integer/parseInt (or users-top-n-arg constants/users-top-n))
        sections-top-n (Integer/parseInt (or sections-top-n-arg constants/sections-top-n))]
    (doseq [file-path [constants/summary-analytics
                       constants/summary-alerts
                       summary]]
      (fs/touch file-path))
    (async/go-loop []
      (let [^Iterator alerts (.iterator (.poll alerts-consumer poll-duration))]
        (while (.hasNext alerts)
          (let [alert (.next alerts)]
            (with-open [writer (io/writer constants/summary-alerts :append true)]
              (.write writer (.value alert))
              (.newLine writer)
              (.flush writer)))))
      (recur))
    (async/go-loop []
      (let [^Iterator analytics (.iterator (.poll analytics-consumer poll-duration))]
        (while (.hasNext analytics)
          (let [analytics ^ConsumerRecord (.next analytics)
                start-time (.key analytics)
                analytics (serde/json->map (.value analytics))]
            (with-open [writer (io/writer constants/summary-analytics :append false)]
              (.write writer (format-analytics users-top-n sections-top-n start-time analytics))
              (.newLine writer)
              (.flush writer)))))
      (recur))
    (async/go-loop []
      (with-open [writer (io/writer summary :append false)]
        (.write writer (slurp constants/summary-alerts))
        (.newLine writer)
        (.write writer (slurp constants/summary-analytics))
        (.newLine writer)
        (.flush writer))
      (when-not @stop?
        (async/<!! (async/timeout (.toMillis poll-duration)))
        (recur)))
    (println "Started sink")
    (async/<!! (async/chan))))
