(ns kafka-log-streams.properties
  (:import (org.apache.kafka.clients.consumer ConsumerConfig)
           (org.apache.kafka.clients.producer ProducerConfig)
           (org.apache.kafka.streams StreamsConfig)
           (java.util Properties)
           (com.KafkaLogStreams TimestampExtractor)
           (org.apache.kafka.common.serialization StringDeserializer StringSerializer Serdes$StringSerde)))

(defn source
  ^Properties [{:keys [kafka-broker-url]}]
  (doto (Properties.)
    (.put ProducerConfig/BOOTSTRAP_SERVERS_CONFIG kafka-broker-url)
    (.put ProducerConfig/MAX_BLOCK_MS_CONFIG 240000)
    (.put ProducerConfig/REQUEST_TIMEOUT_MS_CONFIG (int 120000))
    (.put ProducerConfig/RETRIES_CONFIG (int 3))
    (.put ProducerConfig/KEY_SERIALIZER_CLASS_CONFIG (.getName StringSerializer))
    (.put ProducerConfig/VALUE_SERIALIZER_CLASS_CONFIG (.getName StringSerializer))))

(defn sink
  [{:keys [kafka-brokers group-id offset-reset]}]
  (doto (Properties.)
    (.put ConsumerConfig/BOOTSTRAP_SERVERS_CONFIG kafka-brokers)
    (.put ConsumerConfig/GROUP_ID_CONFIG group-id)
    (.put ConsumerConfig/AUTO_OFFSET_RESET_CONFIG offset-reset)
    (.put ConsumerConfig/KEY_DESERIALIZER_CLASS_CONFIG (.getName StringDeserializer))
    (.put ConsumerConfig/VALUE_DESERIALIZER_CLASS_CONFIG (.getName StringDeserializer))))

(defn analytics
  ^Properties [{:keys [app-id kafka-broker-url]}]
  (doto (Properties.)
    (.put StreamsConfig/PROCESSING_GUARANTEE_CONFIG StreamsConfig/EXACTLY_ONCE)
    (.put ConsumerConfig/AUTO_OFFSET_RESET_CONFIG "earliest")
    (.put ConsumerConfig/MAX_POLL_RECORDS_CONFIG (int 1))
    (.put ConsumerConfig/MAX_POLL_INTERVAL_MS_CONFIG (int 510000))
    (.put ProducerConfig/MAX_BLOCK_MS_CONFIG 240000)
    (.put ProducerConfig/REQUEST_TIMEOUT_MS_CONFIG (int 120000))
    (.put ProducerConfig/RETRIES_CONFIG (int 3))
    (.put StreamsConfig/RETRIES_CONFIG (int 3))
    (.put StreamsConfig/BOOTSTRAP_SERVERS_CONFIG kafka-broker-url)
    (.put StreamsConfig/APPLICATION_ID_CONFIG app-id)
    (.put StreamsConfig/DEFAULT_KEY_SERDE_CLASS_CONFIG (.getName Serdes$StringSerde))
    (.put StreamsConfig/DEFAULT_VALUE_SERDE_CLASS_CONFIG (.getName Serdes$StringSerde))
    (.put StreamsConfig/METRICS_RECORDING_LEVEL_CONFIG "DEBUG")
    (.put StreamsConfig/DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG (.getName TimestampExtractor))))

(def alerts analytics)
