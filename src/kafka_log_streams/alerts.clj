(ns kafka-log-streams.alerts
  (:require [clj-time.coerce :as tc]
            [kafka-log-streams.properties :as properties]
            [kafka-log-streams.serde :as serde]
            [kafka-log-streams.app-parameters :as constants]
            [clojure.core.async :as async]
            [clj-time.core :as t])
  (:import (java.util Properties)
           (org.apache.kafka.streams StreamsBuilder KeyValue Topology KafkaStreams)
           (org.apache.kafka.streams.kstream Transformer TransformerSupplier KStream KeyValueMapper TimeWindows Suppressed Suppressed$BufferConfig Windowed Materialized Predicate Aggregator Initializer ValueJoiner ForeachAction)
           (org.apache.kafka.common.serialization Serdes)
           (org.apache.kafka.streams.state Stores KeyValueStore)
           (java.time Duration)
           (java.lang.reflect Array)
           (org.apache.kafka.streams.processor ProcessorContext)
           (java.time.temporal ChronoUnit)))

(defn window-threshold-situation
  [threshold-high current-rate]
  (if (<= threshold-high current-rate)
    :threshold/above
    :threshold/below))

(defn alert-triggered
  [hits t]
  (format "High traffic generated an alert - hits = %s triggered at %s" (str hits) (str t)))

(defn alert-recovered
  [hits t]
  (format "Traffic now back to usual level - hits = %s triggered at %s" (str hits) (str t)))

(deftype AlertTriggerStatefulMap [^ProcessorContext context
                                  ^String store-name
                                  ^KeyValueStore store
                                  ^Duration duration
                                  ^Long threshold-high]
  Transformer
  (^void init [this ^ProcessorContext context-arg]
    (-> this (. context) (set! context-arg))
    (-> this (. store) (set! (.getStateStore context-arg store-name))))
  (transform [_ k v]
    (let [end-time (tc/from-string k)
          store-key "known-level"
          current-rate (float (/ v (. duration getSeconds)))
          known-level (condp = (.get store store-key)
                        "above" :threshold/above
                        "below" :threshold/below
                        :threshold/below)
          new-level (window-threshold-situation threshold-high current-rate)]
      (condp = [known-level new-level]
        [:threshold/below :threshold/above] (do
                                              (.put store store-key "above")
                                              (KeyValue. k (alert-triggered current-rate end-time)))
        [:threshold/above :threshold/below] (do
                                              (.put store store-key "below")
                                              (KeyValue. k (alert-recovered current-rate end-time)))
        nil)))
  (^void close [_this]
    ;; Kafka Streams API will take care of closing the event-store.
    ))

(defn ^KStream trigger-alert!
  [^KStream stream {:keys [store-name threshold-high duration]}]
  (.transform stream
              (reify TransformerSupplier
                (get [_]
                  (->AlertTriggerStatefulMap nil store-name nil duration threshold-high)))
              (doto (make-array String 1)
                (Array/set 0 store-name))))

(def group-all-value-mapper
  (reify KeyValueMapper
    (apply [_ k v] "ALL")))

(defn topology
  [{:keys [^String access-log
           ^String alerts
           ^Number threshold-high
           ^Duration duration
           ^Duration advance
           ^Properties properties]}]
  (let [builder (StreamsBuilder.)
        alerts-store "alerts-store"
        alerts-store-builder (Stores/keyValueStoreBuilder
                               (Stores/persistentKeyValueStore alerts-store)
                               (serde/key-serde properties)
                               (serde/value-serde properties))]
    (.addStateStore builder alerts-store-builder)
    (-> builder
        (.stream access-log)
        (.groupBy group-all-value-mapper)
        (.windowedBy (-> (TimeWindows/of duration)
                         (.grace (Duration/of 0 ChronoUnit/SECONDS))
                         (.advanceBy advance)))
        (.count (Materialized/with (Serdes/String) (Serdes/Long)))
        (.suppress (Suppressed/untilWindowCloses (Suppressed$BufferConfig/unbounded)))
        (.toStream (reify KeyValueMapper
                     (apply [_ k v]
                       (str (.endTime (.window ^Windowed k))))))
        (trigger-alert! {:store-name alerts-store
                         :threshold-high threshold-high
                         :duration duration})
        (.filter (reify Predicate
                   (test [_ k v]
                     (some? v))))
        (.to alerts))
    (.build builder)))

(defn -main
  [& args]
  (let [[access-log-arg alerts-arg high-traffic-threshold-arg app-id-arg] args
        access-log (or access-log-arg constants/access-log)
        alerts (or alerts-arg constants/alerts)
        high-traffic-threshold (or high-traffic-threshold-arg constants/high-traffic-threshold)
        app-id (or app-id-arg constants/app-id)
        ^Properties properties (properties/alerts {:app-id (format "alerts_%s" app-id)
                                                   :kafka-broker-url "localhost:9092"})
        ^Topology topology (topology {:access-log access-log
                                      :alerts alerts
                                      :duration (Duration/of 2 ChronoUnit/MINUTES)
                                      :advance (Duration/of 1 ChronoUnit/SECONDS)
                                      :threshold-high (Float/parseFloat high-traffic-threshold)
                                      :properties properties})
        ^KafkaStreams stream-task (new KafkaStreams topology properties)]
    (.start stream-task)
    (println "Started alerts")
    (async/<!! (async/chan))))
