(ns kafka-log-streams.source-feed
  (:require [clojure.core.async :as async]
            [clj-time.format :as f]
            [clj-time.core :as t]
            [clj-time.coerce :as tc]
            [kafka-log-streams.serde :as serde]
            [kafka-log-streams.properties :as properties]
            [clj-uuid :as uuid]
            [kafka-log-streams.app-parameters :as constants]
            [clojure.java.io :as io])
  (:import (org.apache.kafka.clients.producer KafkaProducer ProducerRecord)
           (java.io RandomAccessFile)))

(def ^:const ^String read-only "r")

(defn file-worker
  [{:keys [^String file-path
           offset
           poll-ms
           line-worker]}]
  (let [^RandomAccessFile file (RandomAccessFile. file-path read-only)]
    (.seek file ^Long (cond (= offset :offset/earliest) 0
                            (= offset :offset/latest) (.length file)
                            offset offset
                            :default (.length file)))
    (async/go-loop [t (async/timeout poll-ms)]
      (if-let [line (.readLine file)]
        (line-worker line))
      (async/<!! t)
      (recur (async/timeout poll-ms)))))

(def ^:const log-line-pattern
  #"(?<address>.*) - (?<user>.*) \[(?<formattedDate>\d{2}/\w{3}/\d{4}:\d{2}:\d{2}:\d{2} .\d{4})\] \"(?<httpVerb>[^\s]*) (?<requestPath>[^\s]*) (?<protocol>[^\s]*)\" (?<responseCode>\d*) (?<byteSize>\d*)")

(def section-pattern
  #"\/(?<section>[^/]*)")

(def custom-formatter
  (f/formatter "dd/MMM/yyyy:HH:mm:ss Z"))

(defn generate-line
  [now]
  (let [address (rand-nth ["127.0.0.1" "192.168.1.1" "86.82.34.7" "localhost"])
        user (rand-nth ["joana" "ruth" "david" "daisy" "pat" "jill" "peter" "john" "eminem"])
        timestamp (f/unparse custom-formatter now)
        http-verb (rand-nth ["GET" "POST" "PUT" "PATCH" "UPDATE" "DELETE"])
        section (rand-nth ["/api"
                           "/api/user"
                           "/careers/joinus"
                           "/search/lol"
                           "/account/me"
                           "/gdpr/legalese"
                           "/fsf/stallman"
                           "/gnu/cool"
                           "/rms/~perso"])
        protocol (rand-nth ["HTTP/1.1" "HTTP/2.0" "QUIC/3.0"])
        response-code (str (rand-nth [2 3 4 5]) 0 (rand-int 10))
        bytes (str (rand-int 2048))]
    ;; 127.0.0.1 - bella [09/May/2018:16:01:46 +0000] "POST /api/user HTTP/1.0" 503 12
    (if (<= (rand-int 200) 1)
      "I'm badly formatted"
      (str address
           " - "
           user
           " ["
           timestamp
           "] \""
           http-verb
           " "
           section
           " "
           protocol
           "\" "
           response-code
           " "
           bytes))))

(defn -main
  [& args]
  (let [[log-file-arg interval-ms-arg] args
        log-file (or log-file-arg constants/log-file)
        poll-ms (Integer/parseInt (or interval-ms-arg constants/interval-ms-arg))]
    (async/go-loop [t (async/timeout poll-ms)]
      (let [now (t/now)]
        (with-open [writer (io/writer log-file :append true)]
          (dotimes [_ (rand-nth [0 1 10 10 20 20 20 100])]
            (.write writer (generate-line now)))
          (.newLine writer)
          (.flush writer)))
      (async/<!! t)
      (recur (async/timeout poll-ms)))
    (println "Started source feeder, appending to " log-file-arg))
  (async/<!! (async/chan)))
