(ns kafka-log-streams.source
  (:require [clojure.core.async :as async]
            [clj-time.format :as f]
            [clj-time.core :as t]
            [clj-time.coerce :as tc]
            [kafka-log-streams.serde :as serde]
            [kafka-log-streams.properties :as properties]
            [clj-uuid :as uuid]
            [kafka-log-streams.app-parameters :as constants])
  (:import (org.apache.kafka.clients.producer KafkaProducer ProducerRecord)
           (java.io RandomAccessFile)))

(def ^:const ^String read-only "r")

(defn file-worker
  [{:keys [^String file-path
           offset
           poll-ms
           line-worker]}]
  (let [^RandomAccessFile file (RandomAccessFile. file-path read-only)]
    (.seek file ^Long (cond (= offset :offset/earliest) 0
                            (= offset :offset/latest) (.length file)
                            offset offset
                            :default (.length file)))
    (async/go-loop [t (async/timeout poll-ms)]
      (if-let [line (.readLine file)]
        (line-worker line))
      (async/<!! t)
      (recur (async/timeout poll-ms)))))

(def ^:const log-line-pattern
  #"(?<address>.*) - (?<user>.*) \[(?<formattedDate>\d{2}/\w{3}/\d{4}:\d{2}:\d{2}:\d{2} .\d{4})\] \"(?<httpVerb>[^\s]*) (?<requestPath>[^\s]*) (?<protocol>[^\s]*)\" (?<responseCode>\d*) (?<byteSize>\d*)")

(def section-pattern
  #"\/(?<section>[^/]*)")

(def custom-formatter
  (f/formatter "dd/MMM/yyyy:HH:mm:ss Z"))

(defn parse-line
  [line]
  (let [matcher (re-matcher log-line-pattern line)]
    (if (.matches matcher)
      {:address (.group matcher "address")
       :user (.group matcher "user")
       :timestamp (tc/to-long (f/parse custom-formatter (.group matcher "formattedDate")))
       :http-verb (.group matcher "httpVerb")
       :section (last (re-find section-pattern (.group matcher "requestPath")))
       :protocol (.group matcher "protocol")
       :response-code (Integer/parseInt (.group matcher "responseCode"))
       :bytes (Integer/parseInt (.group matcher "byteSize"))}
      {:error "invalid message format"
       :message line
       :timestamp (tc/to-long (t/now))})))

(defn send-kafka!
  [producer topic]
  (fn sender [message]
    (let [timestamp (:timestamp message)
          partition (.partition (rand-nth (.partitionsFor producer topic)))
          partition-key (str (uuid/v4))
          json-message (serde/map->json message)]
      (.send producer (ProducerRecord. ^String topic
                                       ^Integer partition
                                       ^Long timestamp
                                       partition-key
                                       json-message)))))

(defn -main
  [& args]
  (let [[log-file-arg access-log-arg poll-ms-arg] args
        access-log (or access-log-arg constants/access-log)
        log-file (or log-file-arg constants/log-file)
        poll-ms (or poll-ms-arg constants/source-poll-ms)
        producer (KafkaProducer. (properties/source {:kafka-broker-url "localhost:9092"}))]
    (file-worker {:file-path log-file
                  :offset :offset/latest
                  :poll-ms (Integer/parseInt poll-ms)
                  :line-worker (comp (send-kafka! producer access-log)
                                     parse-line)}))
  (println "Started source")
  (async/<!! (async/chan)))
