(ns kafka-log-streams.serde
  "Custom serde. Minimal-effort replacement of Avro schema."
  (:require [clj-uuid :as uuid]
            [jsonista.core :as json]
            [clj-time.coerce :as tc])
  (:import (java.nio ByteBuffer)
           (org.apache.kafka.clients.producer ProducerRecord)
           (java.util UUID Properties)
           (org.apache.kafka.common.serialization Serdes$StringSerde)))

(defn bytes->uuid-v4
  ^UUID [^bytes b]
  (let [buffer ^ByteBuffer (ByteBuffer/wrap b)]
    (uuid/v4 (.getLong buffer) (.getLong buffer))))

(def json-mapper
  (json/object-mapper
    {:encode-key-fn name
     :decode-key-fn keyword}))

(defn record->map
  [^ProducerRecord record]
  {:key (slurp (.key record))
   :value (json/read-value (slurp (.value record)) json-mapper)
   :timestamp (tc/from-long (.timestamp record))})

(defn map->json
  [m]
  (json/write-value-as-string m json-mapper))

(defn json->map
  [m]
  (json/read-value m json-mapper))

(defn key-serde
  [^Properties properties]
  (doto (Serdes$StringSerde.)
    (.configure properties true)))

(defn value-serde
  [^Properties properties]
  (doto (Serdes$StringSerde.)
    (.configure properties false)))
