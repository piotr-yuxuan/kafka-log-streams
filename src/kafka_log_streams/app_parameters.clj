(ns kafka-log-streams.app-parameters)

(def access-log "access-log")
(def alerts "alerts")
(def analytics "alerts")
(def app-id "1")
(def high-traffic-threshold "10") ;; requests per seconds

(def summary-alerts "target/summary_alerts.txt")
(def summary-analytics "target/summary_analytics.txt")
(def summary "target/summary.txt")

(def log-file "/tmp/access.log")

(def users-top-n "3")
(def sections-top-n "3")

(def source-poll-ms "1")
(def interval-ms-arg "20")
