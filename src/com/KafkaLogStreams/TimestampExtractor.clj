(ns com.KafkaLogStreams.TimestampExtractor
  (:require [kafka-log-streams.serde :as serde]
            [clj-time.coerce :as tc])
  (:import (org.apache.kafka.clients.consumer ConsumerRecord))
  (:gen-class
    :name com.KafkaLogStreams.TimestampExtractor
    :implements [org.apache.kafka.streams.processor.TimestampExtractor]))

(defn -extract
  ^Long [_ ^ConsumerRecord record _]
  (-> record
      .value
      serde/json->map
      :timestamp))
